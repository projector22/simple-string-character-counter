# Changelog

## 1.1

- Added argument parser and the argument -t or --text. This takes a string input directly from the app call rather than asking for in.

## 1.0

- Initial functions. App works
