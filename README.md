# Simple String Character Counter

The idea is that a string is inputed or parsed, say *The rain in Spain* and the program returns:

```txt
T h e   r a i n   i  n     S  p  a  i  n
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
```
| Arguments | Results |
| ------ | ------ |
| -t / --text | Imput the string directly into the program. If this argument is not used the program will ask for a string input. **Note** any string with one or more spaces in it must be enclosed with quotation marks ("") |
