#!/usr/bin/env python3

# Author: Gareth Palmer
# Date: 13 October 2019
# Version: 1.1

# This script will take an input string and lay out each character's position numerically

import argparse

def check_space(char, counter):
    if counter >= 0 and counter < 9:
        return char + ' ' 
    elif counter >= 9 and counter < 99:
        if char == ' ':
            return '   '
        else:
            return char + '  '
    else:
        # Works up to 999
        if char == ' ':
            return '     '
        else:
            return char + '   '

ap = argparse.ArgumentParser()
ap.add_argument("-t", "--text", required=False, help="Text to text")
arg = vars(ap.parse_args())

if (arg['text'] != None):
     input_str = arg['text']
else:
    input_str = input('Enter a string to show and count the number of characters in it.\nThis script can deal with texts up to 999 characters.\n\nEnter your string:\n')

disp_one = ''
disp_two = ''
for char in range(0,len(input_str)):
    disp_one = disp_one + check_space(input_str[char], char)
    disp_two = disp_two + str(char+1) + ' '

print(disp_one)
print(disp_two)

print("There are " + str(len(input_str)) + " characters in your string")